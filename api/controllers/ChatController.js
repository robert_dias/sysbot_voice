/**
 * ChatController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var ConversationV1 = require('watson-developer-cloud/conversation/v1');

module.exports = {

  enviarMensagemBot: async function(req, res) {
    if (req.body) {

      var retorno = await sails.helpers.sysbotToWatson.with(req.body);
      await sails.helpers.watsonToChat(retorno);

      res.json(retorno);
    }
  }
};

