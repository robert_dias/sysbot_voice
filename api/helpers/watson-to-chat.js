module.exports = {

  inputs: {
    resposta: {type: 'json'}
  },

  fn: async function(inputs, exists) {
    var chat = {
      canal: 'WEB'
    }

    await Chats.create(chat);

    inputs.resposta.intents.forEach( async function(intent){
      intent.chat = chat._id;

      console.log('---------------------' + JSON.stringify(chat)) ;
      await Intents.create(intent);
    });
    exists.success(true);
  }
};
