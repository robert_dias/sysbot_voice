module.exports = {

  attributes: {
    canal: { type: 'string', required: true},
    intents: {collection: 'intents', via: 'chat'}
  }
};
