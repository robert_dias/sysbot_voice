module.exports = {

  attributes: {

    intent: {type: 'string'},
    confidence: {type: 'number'},

    chat: {
      model: 'chats'
    }
  }
};
